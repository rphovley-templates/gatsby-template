import React, { ReactNode } from 'react'

import { ThemeProvider } from '@material-ui/styles'
import { createMuiTheme } from '@material-ui/core/styles'
// import indigo from '@material-ui/core/colors/indigo'
// import pink from '@material-ui/core/colors/pink'
import red from '@material-ui/core/colors/red'

type ThemeProps = {
  children: ReactNode
  // any other props that come into the component
}

const theme = createMuiTheme({
  palette: {
    // primary: red
    // secondary: {
    // light: '#0a0a'
    //   main: '#0044ff',
    //   // dark: will be calculated from palette.secondary.main,
    //   contrastText: '#ffcc00',
    // },
    // error: red,
    // // Used by `getContrastText()` to maximize the contrast between the background and
    // // the text.
    // contrastThreshold: 3,
    // // Used to shift a color's luminance by approximately
    // // two indexes within its tonal palette.
    // // E.g., shift from Red 500 to Red 300 or Red 700.
    // tonalOffset: 0.2,
  },
})

export default ({ children }: ThemeProps) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
)
