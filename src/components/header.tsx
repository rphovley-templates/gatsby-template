import PropTypes from 'prop-types'
import React, { ReactElement } from 'react'

const Header = ({ siteTitle }): ReactElement => (
  <header
    style={{
      background: 'rebeccapurple',
      marginBottom: '1.45rem',
    }}
  />
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: '',
}

export default Header
