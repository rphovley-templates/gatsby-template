import Typography from 'typography'
import deYoungTheme from 'typography-theme-de-young'

const typography = Typography(deYoungTheme)

export default typography
export const { rhythm } = typography
